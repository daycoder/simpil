from simpil import SimpilImage, CONSOLAS, RED, LEFT, CENTRE, RIGHT, TOP, BOTTOM

image = SimpilImage(width=600,
                    height=300,
                    background_colour=RED,
                    destination='../images/positioning.jpg',
                    font=CONSOLAS[24])

for x in (LEFT, CENTRE, RIGHT):
    for y in (TOP, CENTRE, BOTTOM):
        image.text(text=f'x:{x}\ny:{y}',
                   x=x,
                   y=y)

for x in (150, 400):
    for y in (75, 175):
        image.text(text=f'x:{x}\ny:{y}',
                   x=x,
                   y=y)
