from simpil import SimpilImage, CONSOLAS, RED, WHITE

POEM = """There are holes in the sky
where the rain gets in
but they're ever so small,
That's why rain is thin!"""

image = SimpilImage(width=400,
                    height=200,
                    background_colour=RED,
                    destination='../images/multi_line_text.jpg')

image.text(text=POEM,
           font=CONSOLAS[24],
           colour=WHITE)
