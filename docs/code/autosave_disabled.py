from simpil import SimpilImage

image = SimpilImage(source='https://jpeg.org/images/about.jpg',
                    destination='../images/saved_image.jpg',
                    autosave=False)
