from simpil import SimpilImage, fonts, CONSOLAS, YELLOW, BLUE, LEFT, RIGHT, CENTRE

image = SimpilImage(width=400,
                    height=200,
                    background_colour=BLUE,
                    destination='../images/justified_text_block.jpg',
                    font=CONSOLAS[20])

for justification in (LEFT, CENTRE, RIGHT):
    image.text(text=f'this\ntext\nis\n{justification}\njustified',
               justify=justification,
               x=justification,  # can use LEFT, CENTRE, RIGHT for x
               colour=YELLOW)
