from simpil import SimpilImage, CONSOLAS, RED, TOP, BOTTOM

image = SimpilImage(width=200,
                    height=50,
                    background_colour=RED,
                    destination='../images/scaled_image.jpg',
                    font=CONSOLAS[12])

image.text(text=f'This image was created\n'
                f'as {image.width}x{image.height}.',
           y=TOP)

image.scale(x=2,
            y=4)

# TODO: Figure out why text can't be added after scaling an image
image.text(text=f'This image has been\n'
                f'scaled to {image.width}x{image.height}.',
           y=BOTTOM)
