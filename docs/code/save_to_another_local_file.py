from simpil import SimpilImage, LEFT

image = SimpilImage(source='../images/orange_and_uke.jpg',
                    destination='../images/saved_image.jpg')

image.text(text=("SimpilImage(source='../images/orange_and_uke.jpg',\n"
                 "            destination='../images/saved_image.jpg')"),
           justify=LEFT)
