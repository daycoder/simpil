from simpil import SimpilImage, CONSOLAS, YELLOW, BLUE, LEFT, RIGHT, CENTRE

image = SimpilImage(width=400,
                    height=200,
                    background_colour=BLUE,
                    destination='../images/line_spacing.jpg',
                    font=CONSOLAS[16])

for spacing, x in zip((1, 1.5, 2), (LEFT, CENTRE, RIGHT)):
    image.text(text=f'The spacing\nfor this text\nis {spacing}',
               spacing=spacing,
               x=x,
               colour=YELLOW)
