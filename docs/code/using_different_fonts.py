from simpil import SimpilImage, fonts, RED, BLUE, GREEN, TOP

image = SimpilImage(width=400,
                    height=200,
                    background_colour=RED,
                    destination='../images/using_different_fonts.jpg',
                    font=fonts['consola'][24])

image.text(text='Default Font',
           y=TOP,
           colour=BLUE)

image.text(text='Times 36',
           colour=GREEN,
           font=fonts['times.ttf'][36])
