from simpil import SimpilImage, RED

image = SimpilImage(width=400,
                    height=200,
                    background_colour=RED)

image.save(filename="../images/red_rect.jpg")
