from simpil import SimpilImage, CONSOLAS, BLACK, WHITE, BLUE, LEFT, RIGHT

image = SimpilImage(width=400,
                    height=200,
                    background_colour=BLUE,
                    destination='../images/shadowed_text.jpg',
                    font=CONSOLAS[48])

image.shadowed_text(text=['Shadowed', 'Text'],
                    justify=LEFT,
                    x=RIGHT,
                    colour=WHITE,
                    shadow_colour=BLACK,
                    shadow_size=3)
