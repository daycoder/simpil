from simpil import SimpilImage, CONSOLAS, BLACK, WHITE, BLUE

image = SimpilImage(width=400,
                    height=200,
                    background_colour=BLUE,
                    destination='../images/outlined_text.jpg',
                    font=CONSOLAS[48])

image.outlined_text(text='Outlined\nText',
                    colour=BLACK,
                    outline_colour=WHITE,
                    outline_size=3)
