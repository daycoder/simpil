from simpil import SimpilImage, PNG

image = SimpilImage(source='https://jpeg.org/images/about.jpg')

jpeg_data = image.image_data()
png_data = image.image_data(fmt=PNG)
