.. simpil documentation master file, created by
   sphinx-quickstart on Mon Aug 20 18:01:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :caption: Contents:


simpil
======

Simpil simplifies the loading/creation of images and adding text using PIL/Pillow.


Initialising
------------

There's one class (``SimpilImage``) with several ways to instantiate.

Initialise from a URL
~~~~~~~~~~~~~~~~~~~~~
.. literalinclude :: code/init_from_url.py

.. image :: images/url.jpg

Initialise from a file
~~~~~~~~~~~~~~~~~~~~~~
.. literalinclude :: code/init_from_file.py

.. image :: images/orange_and_uke.jpg

Create a new blank image
~~~~~~~~~~~~~~~~~~~~~~~~

The colour is an RGB tuple.
There are a few predefined constants:
 * ``BLACK``
 * ``WHITE``
 * ``GREY``
 * ``RED``
 * ``GREEN``
 * ``BLUE``
 * ``YELLOW``
 * ``ORANGE``

.. literalinclude :: code/init_new_image.py

.. image :: images/200x100.jpg

Initialise from data
~~~~~~~~~~~~~~~~~~~~
Use ``bytes``, ``BytesIO`` (Py3) or ``StringIO`` (Py2).
You can also use simpil.BytesIO on Py2.

.. literalinclude :: code/init_from_data.py

.. image :: images/200x100.jpg


Saving
------

Automatically saving
~~~~~~~~~~~~~~~~~~~~
By default, saving is automatic when changes are made to a SimpilImage
if it has a source or destination filename.

.. literalinclude :: code/autosave.py

For an image loaded from a local file, changes can be saved to another file.

.. literalinclude :: code/save_to_another_local_file.py

To disable automatic saving, initialise with autosave set to False:

.. literalinclude :: code/autosave_disabled.py


Saving manually
~~~~~~~~~~~~~~~
To save an image, the SimpilImage needs a destination filename.

.. literalinclude :: code/save_image_manually_with_filename.py

When saving back to the same file, you don't need to supply the filename

.. literalinclude :: code/save_image_manually_without_filename.py


Properties
----------

Image data
~~~~~~~~~~
Get the image data in the desired format.
This can be used when there's no need to save the file to disk,
such as for images dynamically created from webserver requests.

.. literalinclude :: code/image_data.py


Width and height
~~~~~~~~~~~~~~~~
Width and height properties are available:

.. literalinclude :: code/properties.py


Scaling
-------

Scale by using ``x`` and ``y``. Use a value of ``2`` to double the size.
If only one of `x` or `y` is used, the aspect ratio is retained.
If you want to scale on one axis only, set the other to ``1`.

.. literalinclude :: code/scale_image.py

.. image :: images/scaled_image.jpg

Note from this example that there is an issue with adding text after
an image is scaled.

Adding Text
-----------

There are a few simple ways to add text to an image...

Single line
~~~~~~~~~~~

Use ``SimpilImage.text()`` to add text to an image.

.. literalinclude :: code/add_text_to_an_image.py

This example shows the default font. The default ``colour`` is ``WHITE``.

.. image :: images/add_text.jpg


Declaring fonts
~~~~~~~~~~~~~~~
You can you use instances of ``PIL.ImageFont``, but an
easier way is to use the ``fonts`` object. This caches
font instances in one place, accessed by name and size (in pixels).
The name must match the filename, but you can often omit the
file extension (``.ttf`` and ``.otf``).

.. literalinclude :: code/declaring_fonts.py


Using different fonts
~~~~~~~~~~~~~~~~~~~~~
Fonts can be supplied as a default for the SimpilImage
or discretely to each text function:

.. literalinclude :: code/using_different_fonts.py

.. image :: images/using_different_fonts.jpg

Note how different colours are used in this example.


Multiline Text
~~~~~~~~~~~~~~
Newlines in strings will be respected. You can also use a list of strings,
but if those strings should not contain newlines.

.. literalinclude :: code/add_multi_line_text_to_an_image.py

.. image :: images/multi_line_text.jpg


Positioning Text on the Image
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
You can specify positions ``x`` which can use constants ``LEFT``, ``CENTRE`` or ``RIGHT``
and `y` which can use the constants ``TOP``, ``CENTRE`` or ``BOTTOM``.
``x`` and ``y`` can also take absolute values for the top left of the text.

.. literalinclude :: code/positioning_text.py

.. image :: images/positioning.jpg


Justifying a block of text
~~~~~~~~~~~~~~~~~~~~~~~~~~
You can control how the text is justified by passing value to ``justify``.
This should be ``LEFT``, ``CENTRE`` or ``RIGHT``. The default is ``CENTRE``.

.. literalinclude :: code/justified_text_block.py

.. image :: images/justified_text_block.jpg


Linespace in a block of text
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
By default, there's no gap between lines, so descenders might meet ascenders.
If you want a bit more spce, use a value for ``spacing``.
The default is ``1``. Using``1.5`` adds a spacing of half a line height between each line.

.. literalinclude :: code/line_spacing.py

.. image :: images/line_spacing.jpg


Text with a Drop Shadow
~~~~~~~~~~~~~~~~~~~~~~~

Use ``SimpilImage.shadowed_text()`` to add text to an image with a bottom-right shadow.
Useful for making text stand out again a background that is similar to the text colour

.. literalinclude :: code/shadowed_text.py

.. image :: images/shadowed_text.jpg

Note how the colour of the shadow can be set with ``shadow_colour``
and the shadow depth with ``shadow_size``.


Text with an Outline
~~~~~~~~~~~~~~~~~~~~
Use ``SimpilImage.outlined_text()`` to add plain text to an image with an outline.
Useful for making text stand out against a background that is similar to the text colour

.. literalinclude :: code/outlined_text.py

.. image :: images/outlined_text.jpg

Note how the colour of the outline can be set with ``outline_colour``
and the outline size with ``outline_size``.

