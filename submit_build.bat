rmdir build /S /Q
rmdir dist /S /Q
erase *.egg-info /Q
python setup.py sdist
python setup.py bdist_wheel
twine upload dist/*